# Use

Create a folder (e.g. openpatch). Clone this repo into the folder.

`git clone https://gitlab.com/openpatch/local-environment.git`

Your folder structure should look like this:

- openpatch
  - local-environment

Run `./init.sh`. This script will clone all repositories of openpatch services
to your device. Your folder structure will look like this:

- openpatch
  - local-environment
  - authentification-backend
  - assessment-backend
  - comment-backend
  - ...

In the folder `local-environment` run `docker-compose up`. This will run all OpenPatch
services in development mode. If you modify a file in e.g. `itembank-backend`
the `itembank-backend` service will automatically reload.

# Databases

For this setup all services use SQLite3 database. This makes it easy to reset the database of a service by using `git checkout`, to view the content of the database and to modify it by using e.g. [SQLiteBrowser](https://sqlitebrowser.org/)

The database bases and other service generated files are located in the `.openpatch` folder.

# User

| Username | Password | Email                 |
| -------- | -------- | --------------------- |
| admin    | admin    | admin@openpatch.org   |
| analyst  | analyst  | analyst@openpatch.org |
| editor   | editor   | editor@openpatch.org  |
| user     | user     | user@openpatch.org    |

You can create your own user by signing up (http://localhost:3000/sign-up).

You should see the mail on in the log of the mail service.

`docker-compose logs mail`

It should look like this:

```
INFO:root:Sending mail: {&quot;from&quot;: &quot;OpenPatch &lt;noreply@mail.openpatch.app&gt;&quot;, &quot;to&quot;: [&quot;mike@openpatch.org&quot;], &quot;subject&quot;: &quot;Confirm your email&quot;, &quot;text&quot;: &quot;Hello mike! Once you&apos;ve confirmed your email address, you&apos;ll be the newest member of the OpenPatch community.\n\nhttp://localhost:3000/confirm-mail?token=Im1pa2VAb3BlbnBhdGNoLm9yZyI.YAsnpw.T0vkYfYYLYsWofFflvkXChmMuyM\nThis link is valid for 1 day.&quot;, &quot;html&quot;: null}
```
