#!/bin/bash

LOCAL_DEV=$PWD


echo "Init"
echo $PATH

REPOS="assessment-backend authentification-backend comment-backend format-backend itembank-backend media-backend mail-backend recorder-backend runner web-frontend"
DOCKER_NETWORKS="openpatch-runner-net"

for docker_network in ${DOCKER_NETWORKS}; do
  echo ""
  echo -e "\e[34mInit Network: ${docker_network}"
  echo -e "\e[0m"
  docker network create ${docker_network}
done

for repo in ${REPOS}; do
  echo ""
  echo -e "\e[34mInit: ${repo}"
  echo -e "\e[0m"
  cd .. && git clone "git@gitlab.com:openpatch/${repo}.git" && cd $LOCAL_DEV
done
